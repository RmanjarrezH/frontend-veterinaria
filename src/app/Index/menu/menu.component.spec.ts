import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuComponent } from './menu.component';
import { EventEmitter } from '@angular/core';
import { menuOptions } from 'src/types/types';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('opcion debe estar definido como EventEmitter', () => {
    expect(component.opcion).toBeDefined(EventEmitter);
  });

  it('seleccionado debe estar definido como "inicio"', () => {
    expect(component.opcion).toBeDefined('inicio');
  });

  it('cambiar opcion debe emitir opciones de menuOptions', () => {
    let cases: menuOptions[] = ["inicio", "servicios", "productos", "mision-vision", "contactos"];
    spyOn(component.opcion, 'emit');
    cases.forEach(item => {
      component.cambiarOpcion(item);
      expect(component.opcion.emit).toHaveBeenCalledWith(item);
    });
  });
});
