import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import 'hammerjs';
import { index } from "./index.component";
import { menuOptions } from 'src/types/types';

describe('index', () => {
    let component: index;
    let fixture: ComponentFixture<index>;
    let menuOptionsCases: menuOptions[] = ['inicio' , 'productos' , 'servicios' , 'mision-vision' , 'contactos'];

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [ index ]
      })
      .compileComponents();
    }));
  
    beforeEach(() => {
      fixture = TestBed.createComponent(index);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('opcionMenu debe comenzar con inicio', () => {
      expect(component.opcionMenu).toBeDefined('inicio');
    });

    it('verOpcion debe cambiar el estado de opcionMenu', () => {
      menuOptionsCases.forEach(item => {
        component.verOpcion(item);
        expect(component.opcionMenu).toEqual(item);
      });
    });
  });
  