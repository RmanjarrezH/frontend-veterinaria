import { Component } from "@angular/core";
import { menuOptions } from 'src/types/types';

@Component({
    selector: 'index-app',
    templateUrl: 'index.component.html',
    styleUrls: ['index.component.css']
})

export class index {
    opcionMenu: menuOptions = 'inicio';

    verOpcion(event: menuOptions) {
        this.opcionMenu = event;
    }
}
