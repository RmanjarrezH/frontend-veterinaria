import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { index } from "./index.component";
import { angularMaterialModule } from '../angularMaterial.module';
import { MenuComponent } from './menu/menu.component';
import { ProductosComponent } from './productos/productos.component';
import { ContactoComponent } from './contacto/contacto.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { indexRoutesModule } from './index.routes';
import { InicioComponent } from './inicio/inicio.component';

@NgModule({
    declarations: [
        index,
        MenuComponent,
        InicioComponent,
        ProductosComponent,
        ContactoComponent,
        ServiciosComponent
    ],
    imports: [
        indexRoutesModule,
        FormsModule,
        ReactiveFormsModule,
        angularMaterialModule,
        CommonModule
    ],
    exports: [],
    entryComponents: [],
    bootstrap: [index]
})

export class indexModule {}