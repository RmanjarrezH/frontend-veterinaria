import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  
  direccion: string = 'Cra # 19-05, Valledupar, Cesar, Colombia';
  contacto: string = '3116573932';

  constructor() { }

  ngOnInit() {
  }
  
}