import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { index } from './index.component';
import { InicioComponent } from './inicio/inicio.component';

const routes: Routes = [
    { path: '', component: index }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class indexRoutesModule {}